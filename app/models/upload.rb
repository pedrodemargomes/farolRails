class Upload < ApplicationRecord
  resourcify 

  has_and_belongs_to_many :labels
  belongs_to :user
end
