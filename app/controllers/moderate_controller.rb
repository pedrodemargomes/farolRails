class ModerateController < ApplicationController
	load_and_authorize_resource

    before_action :authenticate_user!

	def index
		@documents = Upload.where(approved: false)
	end

	def show_approved
		@documents = Upload.where(approved: true)
	end

end
