class CreateJoinTableLabelUpload < ActiveRecord::Migration[5.1]
  def change
    create_join_table :Labels, :Uploads do |t|
      # t.index [:label_id, :upload_id]
      # t.index [:upload_id, :label_id]
    end
  end
end
