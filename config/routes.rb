Rails.application.routes.draw do
  devise_for :users
  get 'search/index', to: 'search#index'
  get 'search/show/:id', to: 'search#show'

  get 'upload', to: 'upload#index'
  post 'upload/create', to: 'upload#create'
  get 'moderate/edit/:id', to: 'upload#edit'
  post 'moderate/edit/:id', to: 'upload#update'

  get 'moderate', to: 'moderate#index'
  delete 'moderate', to: 'upload#delete'
  post 'moderate/approve', to: 'upload#approve'
  get 'moderate/show_approved', to: 'moderate#show_approved'


  root 'search#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
