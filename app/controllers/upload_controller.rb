class UploadController < ApplicationController

	load_and_authorize_resource

    before_action :authenticate_user!

	def index

	end

	def create
		@document = Upload.new(params[:document].permit(:teacher, :subject, :year, :typedoc))
	    @document.file = @document.typedoc+"_"+@document.subject+"_"+@document.teacher+"_"+@document.year.to_s+"_"+Time.now.to_s + Time.now.usec.to_s+".pdf"
		File.open("db/documents/"+@document.file, 'wb') { |f| f.write( params[:document][:datafile].read ) }
		
		@document.user = current_user
		@document.approved = false

		@label0nome = params[:document][:labels0]
		@label1nome = params[:document][:labels1]
		
		if not @document.save
			render "create"
		end

		if @label0nome != ""
			# Checar label antes de aprova-lá
			@document.labels.create(nome: @label0nome, status: true ) 
		end

		if @label1nome != ""
			# Checar label antes de aprova-lá
			@document.labels.create(nome: @label1nome, status: true ) 
		end	

		redirect_to "/upload"
	end
  
	def delete
		@document = Upload.find(params[:id])
		File.delete("db/documents/"+@document.file);
		@document.destroy
	end
	
	def approve
		@document = Upload.find(params[:id])
		@document.approved = true
		@document.save
	end
	
	def edit
		@document = Upload.find(params[:id])
	end
	
	def update
		
		@document = Upload.find(params[:id])
		if not @document.update(params[:document].permit(:teacher, :subject, :year, :typedoc))
			render "edit"
		end

		redirect_to "/moderate"
	end
end
