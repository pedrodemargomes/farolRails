class SearchController < ApplicationController
 
  def index
    @documents = Upload.where(approved: true)
	@current_user = current_user
  end

  def show
    @document = Upload.where(id: params[:id])
    if @document.exists?
	  send_file(File.open("db/documents/"+@document[0].file), filename: @document[0].file, type: 'application/pdf', disposition: :inline )
	else
	  render plain: "Documento não encontrado", content_type: "text/plain"
	end

  end

end
